# terraform-public

public facing folder for terraform projects

# DigitalOcean-RHCE-Lab

This folder contains a simple Terraform plan which quickly spins up a cheap redhat certified engineer test lab environment for exam prep. I also assume that you are keeping all your secrets used for provisioning within a Hashicorp Vault. I'm also assuming that you are using tailscale as your DNS/VPN providor.  

This terraform plan relies on a variables.tf file which needs to be provisioned with the following information:
```
variable "vault_key" {
  default = "Key you use to unlock your Hashicorp Vault"
}
variable "vault_location" {
  default = "The URL of your Hashicorp Vault"
}
variable "vault_path" {
  default = "The path to your secrets"
}
variable "private_key" {
  default = "The Private Key you want to use to connect to your Digital Ocean Droplets"
}
```