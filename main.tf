terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.10"
    }
    vault = {
        source = "hashicorp/vault"
        version = "3.6.0"
    }
  }
}
provider "vault" {
  address = "http://vault.littleseneca.lan:8200"
  skip_tls_verify = true
  token = "PUT YOUR TOKEN HERE"
}
data "vault_generic_secret" "proxmox_secret" {
  path = "littleseneca.lan/proxmox"
}
data "vault_generic_secret" "ansible_secret" {
  path = "littleseneca.lan/ansible"
}
provider "proxmox" {
  pm_api_url = "https://proxmox.littleseneca.lan:8006/api2/json"
  pm_api_token_id = data.vault_generic_secret.proxmox_secret.data["proxmox_id"]
  pm_api_token_secret = data.vault_generic_secret.proxmox_secret.data["proxmox_key"]
  pm_tls_insecure = true
}
resource "proxmox_vm_qemu" "test_environment" {
  vmid              = "30${count.index}"
  count             = 4
  name              = "tf-vm-${count.index}"
  target_node       = "shiloh"
  clone             = "AlmaLinux-8-template"
  os_type           = "cloud-init"
  cores             = 1 
  sockets           = "1"
  cpu               = "host"
  memory            = 2048
  scsihw            = "virtio-scsi-pci"
  bootdisk          = "scsi0"
  agent             = 1
  disk {
    size            = "20G"
    type            = "scsi"
    storage         = "local-lvm"
    iothread        = 1
  }
  network {
    model           = "virtio"
    bridge          = "vmbr0"
  }
  lifecycle {
    ignore_changes  = [
      network,
    ]
  }
  # Cloud Init Settings
  ipconfig0 = "ip=192.168.3.10${count.index + 1}/16,gw=192.168.0.1"
  searchdomain = "littleseneca.lan"
  nameserver = "192.168.2.100 8.8.8.8"
  ciuser = data.vault_generic_secret.ansible_secret.data["ansible_username"]
  cipassword = data.vault_generic_secret.ansible_secret.data["ansible_password"] 
  # Domain Join Process
  connection {
    type = "ssh"
    user = data.vault_generic_secret.ansible_secret.data["ansible_username"]
    private_key = "${file("/home/administrator/.ssh/id_rsa")}"
    host = "192.168.3.10${count.index + 1}" 
  }
  provisioner "remote-exec" {
    inline = [
        "sudo dnf clean packages -y",
        "sudo dnf install freeipa-client curl -y",
        "sudo dnf clean packages -y",
        "sudo dnf install freeipa-client curl -y",
        "sudo ipa-client-install --mkhomedir --enable-dns-updates --unattended --password=\"${data.vault_generic_secret.ansible_secret.data["idm_password"]}\" --principal=\"${data.vault_generic_secret.ansible_secret.data["idm_username"]}\"",
        "curl --location --request POST 'http://builder.littleseneca.lan/api/v2/groups/1/hosts/' --header \"${data.vault_generic_secret.ansible_secret.data["ansible_api_key"]}\" --header 'Content-Type: application/json' --data-raw '{\"name\": \"tf-vm-${count.index}\",\"description\": \"\",\"enabled\": true,\"instance_id\": \"\",\"variables\": \"\"}'"
    ]
  }
}
resource "proxmox_vm_qemu" "seedbox" {
  vmid              = "203"
  count             = 1
  name              = "seed.littleseneca.lan"
  target_node       = "shiloh"
  clone             = "AlmaLinux-8-template"
  os_type           = "cloud-init"
  cores             = 2 
  sockets           = "1"
  cpu               = "host"
  memory            = 4098
  scsihw            = "virtio-scsi-pci"
  bootdisk          = "scsi0"
  agent             = 1
  disk {
    size            = "20G"
    type            = "scsi"
    storage         = "local-lvm"
    iothread        = 1
  }
  network {
    model           = "virtio"
    bridge          = "vmbr0"
  }
  lifecycle {
    ignore_changes  = [
      network,
    ]
  }
  # Cloud Init Settings
  ipconfig0 = "ip=192.168.2.103/16,gw=192.168.0.1"
  searchdomain = "littleseneca.lan"
  nameserver = "192.168.2.100 8.8.8.8"
  ciuser = data.vault_generic_secret.ansible_secret.data["ansible_username"]
  cipassword = data.vault_generic_secret.ansible_secret.data["ansible_password"] 
  # Domain Join Process
  connection {
    type = "ssh"
    user = data.vault_generic_secret.ansible_secret.data["ansible_username"]
    private_key = "${file("/home/administrator/.ssh/id_rsa")}"
    host = "192.168.2.103" 
  }
  provisioner "remote-exec" {
    inline = [
        "sudo dnf clean packages -y",
        "sudo dnf install freeipa-client curl -y",
        "sudo dnf clean packages -y",
        "sudo dnf install freeipa-client curl -y",
        "sudo ipa-client-install --mkhomedir --enable-dns-updates --unattended --password=\"${data.vault_generic_secret.ansible_secret.data["idm_password"]}\" --principal=\"${data.vault_generic_secret.ansible_secret.data["idm_username"]}\"",
        "curl --location --request POST 'http://builder.littleseneca.lan/api/v2/groups/1/hosts/' --header \"${data.vault_generic_secret.ansible_secret.data["ansible_api_key"]}\" --header 'Content-Type: application/json' --data-raw '{\"name\": \"seed.littleseneca.lan\",\"description\": \"\",\"enabled\": true,\"instance_id\": \"\",\"variables\": \"\"}'"
    ]
  }
}