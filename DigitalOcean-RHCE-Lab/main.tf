terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    vault = {
      source = "hashicorp/vault"
      version = "3.7.0"
    }
  }
}
provider "vault" {
  address = var.vault_location
  token = var.vault_key
}
data "vault_generic_secret" "terraform" {
  path = var.vault_path
}
data "digitalocean_ssh_key" "LittleSeneca" {
  name = "LittleSeneca"
}
provider "digitalocean" {
  token = data.vault_generic_secret.terraform.data["do_api_token_key"]
}
resource "digitalocean_droplet" "rhce-1" {
  count = 4
  image = "rockylinux-8-x64"
  name = "rhce-${count.index}"
  region = "sfo3"
  size = "s-1vcpu-1gb"
  ssh_keys = [
    data.digitalocean_ssh_key.LittleSeneca.id
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.private_key)
    timeout = "2m"
  }
  provisioner "remote-exec" {
    inline = [
      "dnf install dnf-plugins-core -y",
      "dnf config-manager --add-repo https://pkgs.tailscale.com/stable/centos/8/tailscale.repo",
      "dnf install tailscale -y",
      "systemctl enable --now tailscaled",
      "tailscale up --authkey ${data.vault_generic_secret.terraform.data["ts_api_token_key"]}"
    ]
  }
  tags = ["dev"]
}